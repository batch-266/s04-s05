package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        Dog iro = new Dog();
        iro.setName("Doggo");
        iro.setColor("Red");
        iro.speak();

        System.out.println(iro.getName() + " " + iro.getBreed() + iro.getColor() );
        Dog myDreamPet = new Dog();
        myDreamPet.setName("Iro");
        myDreamPet.setColor("Black");
        myDreamPet.speak();
        System.out.println(myDreamPet.getName() + " " + myDreamPet.getBreed() + " " + myDreamPet.getColor());

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2025);
        System.out.println("Car Name: " + myCar.getName());
        System.out.println("Car Brand: " + myCar.getBrand());
        System.out.println("Car Year of Make: " + myCar.getYearOfMake());
        System.out.println("Car Driver: " + myCar.getDriverName());

        //Abstraction
        //is a process where all the logic and complexity are hidden from the user
        Person child = new Person();
        child.run();
        child.sleep();

        //Polymorphism
        //Delivered from the greek word: poly means "many" and morph means "form"
        StaticPoly myAddition = new StaticPoly();

        System.out.println(myAddition.addition(5,6));
        System.out.println(myAddition.addition(5,6, 10));
        System.out.println(myAddition.addition(5.5,6.5));
    }
}
